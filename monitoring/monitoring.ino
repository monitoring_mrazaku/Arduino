#include <ESP8266WiFi.h>
#include <ESP8266ZabbixSender.h>
#include <DHT.h>
//#include "konfigurace_local.h"
#include "konfigurace.h"

ESP8266ZabbixSender zSender;
DHT dht(DHT_pin, DHT11);

void odesli_zpravu(){
  float napeti_baterie = (analogRead(BATERIE_pin) * 4.8)/1024; // napeti baterie
  float teplota = dht.readTemperature();  // teplota
  float vlhkost = dht.readHumidity();   // vlhkost
  int kontakt = (digitalRead(MRAZAK_pin) == 0) ? 1 : 0; // stav kontaktu
  
  // Vypiseme vse do konzole (pro debug)
  Serial.print("Napeti baterie(V): ");
  Serial.println(napeti_baterie);
  Serial.print("Teplota (°C): ");
  Serial.println(teplota);
  Serial.print("Vlhkost (%): ");
  Serial.println(vlhkost);
  Serial.print("Stav kontaktu (0/1): ");
  Serial.println(kontakt);  
  
  zSender.ClearItem();
  zSender.AddItem("kontakt", kontakt);
  zSender.AddItem("baterie", napeti_baterie);
  zSender.Send();
  zSender.ClearItem();
  zSender.AddItem("teplota", teplota);
  zSender.AddItem("vlhkost", vlhkost);
  zSender.Send();
}

void setup() {
  Serial.begin(115200);
  pinMode(MRAZAK_pin, INPUT_PULLUP);
  Serial.println("Start");
  //WiFi.config(staticIP, gateway, subnet);
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(100);
  }
  Serial.println("Pripojeno");
  zSender.Init(IPAddress(SERVERADDR), ZABBIXPORT, ZABBIXAGHOST);
  odesli_zpravu();
  ESP.deepSleep(zpozdeni * 1000000);  
}

void loop() {
}

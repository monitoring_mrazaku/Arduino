#define BATERIE_pin A0 // Port na kterem je pripojena baterie pres odpor 150 
#define MRAZAK_pin D5 // Port na kterem je pripojeny mrazak
#define DHT_pin D6 // Port na kterem je pripojene DHT11

#define SERVERADDR 10, 87, 0, 136 // IP zabbix serveru
#define ZABBIXPORT 10051      // Port zabbix serveru
#define ZABBIXAGHOST "FIXME"  // Nazev itemu v zabbixu

IPAddress staticIP(192, 168, 0, 143); // IP Adresa zarizeni - FIXME
IPAddress gateway(192, 168, 0, 1); // Brana zarizeni
IPAddress subnet(255, 255, 255, 0); // Maska zarizeni 

const int zpozdeni = 120; // cas v sekundach, kdy bude zarizeni v deep sleep modu

const char* ssid     = "FIXME"; // SSID
const char* password = "FIXME"; // Heslo k wifi
